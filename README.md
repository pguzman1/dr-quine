# Quine � A self-reproducing program
* A quine is a program which prints a copy of its own as the only output. A quine takes no input. Quines are named after the American mathematician and logician Willard Van Orman Quine (1908�2000). The interesting thing is you are not allowed to use open and then print file of the program.

# What you will find and what you can do: 

```sh
$  clang -Wall -Wextra -Werror -o Colleen Colleen.c; ./Colleen > tmp_Colleen ; diff tmp_Colleen Colleen.c
$ 
```

```sh
$  clang -Wall -Wextra -Werror -o Grace Grace.c; ./Grace ; diff Grace.c Grace_kid.c
$ 
```

```sh
$ clang -Wall -Wextra -Werror ../Sully.c -o Sully ; ./ Sully
$ diff ../Sully.c Sully_0.c
1c1
< int i = 5;
---
> int i = 0;
$ diff Sully_3.c Sully_2.c
1c1
< int i = 3;
---
> int i = 2;
$
```


```sh
$  python Colleen.py > tmp_Colleen ; diff tmp_Colleen Colleen.py
$ 
```

```sh
$  python Grace.py; diff Grace.py Grace_kid.py
$ 
```

```sh
$ python Sully.py
$ diff ../Sully.py Sully_0.py
1c1
< int i = 5;
---
> int i = 0;
$ diff Sully_3.py Sully_2.py
1c1
< int i = 3;
---
> int i = 2;
$
```
